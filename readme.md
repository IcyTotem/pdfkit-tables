Please refer to [this post](https://www.andronio.me/2017/09/02/pdfkit-tables/) for more information on how to use this code snippet.

Run `node example.js` for a pdf sample of the table output.
